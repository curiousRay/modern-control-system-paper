\contentsline {section}{\numberline {0}引言}{1}% 
\contentsline {section}{\numberline {1}状态平均法原理}{1}% 
\contentsline {section}{\numberline {2}建模过程}{2}% 
\contentsline {subsection}{\numberline {2.1}晶体管导通的情况($0 \leqslant t \leqslant t_{on}$)}{3}% 
\contentsline {subsection}{\numberline {2.2}晶体管截止的情况($t_{on} \leqslant t \leqslant T$)}{3}% 
\contentsline {subsection}{\numberline {2.3}进行状态平均}{4}% 
\contentsline {section}{\numberline {3}仿真过程}{5}% 
\contentsline {subsection}{\numberline {3.1}稳态输出验证}{5}% 
\contentsline {subsection}{\numberline {3.2}理论公式验证}{7}% 
\contentsline {paragraph}{改变输入电压}{7}% 
\contentsline {paragraph}{改变占空比}{7}% 
\contentsline {section}{\numberline {4}研究体会}{9}% 

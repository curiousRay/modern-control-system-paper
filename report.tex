\documentclass[12pt, a4paper]{ctexart}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{listings}
\usepackage[framed,numbered,autolinebreaks,useliterate]{mcode}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{float}
\usepackage{geometry}
\geometry{left=3.18cm,right=3.18cm,top=3.18cm,bottom=3.18cm}
\newcommand{\HUGE}{\fontsize{29pt}{29pt}\selectfont} 
\CTEXsetup[format={\Large\bfseries}]{section}

%opening
\title{}
\author{}

\begin{document}

\begin{titlepage} 
	% 首行的位置往上调整。但vspace前面需要有东西才会起效。
	\phantom{Start!}
	\vspace{-1.7cm} 
	\begin{flushleft}
		\emph{\Large 南京航空航天大学}\\[4.2cm] 
		% Title
		{ \Large \bfseries 现代控制理论}\\[0.4cm]
		{ \HUGE \bfseries Buck-Boost变换器}\\[0.2cm]
		{ \HUGE \bfseries 状态空间平均法建模分析}\\[0.6cm]
		{ \huge \bfseries 大作业报告} 
	\end{flushleft}
	
	\vfill 
	
	\begin{flushright}
		{
			% \pillar：使用一种统一的方法提高行高
			\newcommand{\pillar}{ {\Huge \phantom{A}} }
			\large
			\begin{tabular}{lc}
				\pillar 姓名 & 雷砺豪 \\\cline{2-2}
				\pillar 学号 & 031610118 \\\cline{2-2}
				\pillar 班级 & 0316101 \\\cline{2-2}
				\pillar 报告日期 & 2019年5月23日 \\\cline{2-2}
			\end{tabular}
		} 
	\end{flushright} 
\end{titlepage}

\setcounter{section}{-1}

\tableofcontents

\section{引言}
Buck-Boost变换器是一种使用晶体管作为功率开关的直流-直流变换电路。其特点是结构简单，效率很高。通过改变晶体管周期性导通的占空比，我们可以控制变换器处于降压（Buck）或升压(Boost)的工作状态。对开关变换器的建模分析有助于优化开关电源的性能。本文将以\textbf{电感电流连续导通}的情况为例进行建模。根据电力电子领域的理论推导Buck-Boost变换器在此条件下，输出电压与输入电压有如下关系：
$$
U_{o}=\cfrac{DU_{i}}{D-1}
$$

\section{状态平均法原理}
在Buck-Boost变换电路中，取电容电压、电感电流为状态变量。由于功率开关器件有导通和截止两种状态，且电路在一个周期内都要经历这两种状态。每种状态下电路拓扑结构不同，因而对应的状态空间模型不同。利用状态平均技术，我们可以得到一个周期内的平均状态变量，从而将非线性的电路等效为线性电路，降低分析难度。

\begin{figure}[H]
	\centering
	\includegraphics[width=2.6in]{fig/scope.eps}
\end{figure}

\section{建模过程}
Buck-Boost变换器电路：
\begin{figure}[H]
	\centering
	\includegraphics[height=2.4in]{fig/原电路.eps}
\end{figure}
定义变量：
\begin{center}
	$i_{L}$ 电感电流 \\
	$u_{C}$ 电容电压 \\
	$x=\begin{bmatrix}
		i_{L} & u_{C}
	\end{bmatrix}^\mathrm{T}$ 状态变量 \\
	$T$ 开关周期 \\
	$D$ 占空比 \\
	$t_{on}=DT$ 导通时间 \\
\end{center}

\subsection{晶体管导通的情况($0 \leqslant t \leqslant t_{on}$)}
等效电路：
\begin{figure}[H]
	\centering
	\includegraphics[height=2.5in]{fig/导通.eps}
\end{figure}
列写电路方程组：

$$
\begin{cases}
 L\cfrac{\mathrm{d}i_{L}}{\mathrm{d}t}=V \\
 C\cfrac{\mathrm{d}u_{C}}{\mathrm{d}t}+\cfrac{u_{o}}{R}=0 \\
 u_{o}=u_{C}
\end{cases}
\Rightarrow
\begin{cases}
\cfrac{\mathrm{d}i_{L}}{\mathrm{d}t}=\cfrac{V}{L} \\
\cfrac{\mathrm{d}u_{C}}{\mathrm{d}t}=-\cfrac{u_{C}}{RC}
\end{cases}
$$

状态空间方程：
$$
\begin{cases}
	\dot{x}=A_{1}x+B_{1}v \\
	y=C_{1}x
\end{cases}
$$

其中：
$$
A_{1}=
\begin{bmatrix}
0 & 0 \\
0 & -\cfrac{1}{RC}
\end{bmatrix},
B_{1}=
\begin{bmatrix}
\cfrac{1}{L} \\ 0
\end{bmatrix},
C_{1}=\begin{bmatrix}
	0 & 1
\end{bmatrix}
$$

\subsection{晶体管截止的情况($t_{on} \leqslant t \leqslant T$)}
等效电路：
\begin{figure}[H]
	\centering
	\includegraphics[height=2.5in]{fig/截止.eps}
\end{figure}

列写电路方程组：

$$
\begin{cases}
L\cfrac{\mathrm{d}i_{L}}{\mathrm{d}t}=u_{o} \\
C\cfrac{\mathrm{d}u_{C}}{\mathrm{d}t}+\cfrac{u_{o}}{R}+i_{L}=0 \\
u_{o}=u_{C}
\end{cases}
\Rightarrow
\begin{cases}
\cfrac{\mathrm{d}i_{L}}{\mathrm{d}t}=\cfrac{u_{c}}{L} \\
\cfrac{\mathrm{d}u_{C}}{\mathrm{d}t}=-\cfrac{-u_{C}}{RC}-\cfrac{i_{L}}{C}
\end{cases}
$$

状态空间方程：
$$
\begin{cases}
\dot{x}=A_{2}x+B_{2}v \\
y=C_{2}x
\end{cases}
$$

其中：
$$
A_{2}=
\begin{bmatrix}
0 & \cfrac{1}{L} \\
-\cfrac{1}{C} & -\cfrac{1}{RC}
\end{bmatrix},
B_{2}=
\begin{bmatrix}
0 \\ 0
\end{bmatrix},
C_{2}=\begin{bmatrix}
0 & 1
\end{bmatrix}
$$

\subsection{进行状态平均}
对上述两种情况进行状态平均。令：
\begin{center}
	$A=DA_{1}+(1-D)A_{2}$ \\
	$B=DB_{1}+(1-D)B_{2}$ \\
	$C=C_{1}=C_{2}$
\end{center}

可得线性系统：
$$
A=
\begin{bmatrix}
0 & \cfrac{1-D}{L} \\
-\cfrac{1-D}{C} & -\cfrac{D}{RC}
\end{bmatrix},
B=
\begin{bmatrix}
\cfrac{D}{L} \\ 0
\end{bmatrix},
C=\begin{bmatrix}
0 & 1
\end{bmatrix}
$$

注意：此处上下文中的$D$表示加在晶体管栅极的方波信号的占空比。
\section{仿真过程}
\subsection{稳态输出验证}
设置仿真参数：$R=20\Omega$, $L=200\mathrm{\mu H}$, $C=470\mathrm{\mu F}$, $V=50\mathrm{V}$, $D=0.5$。

MATLAB程序：
\begin{lstlisting}
D = 0.5;%D为占空比
L = 200*10^(-6);
R = 20;
Cap = 470*10^(-6);
V = 50;

A = [0 (1-D)/L; -(1-D)/Cap -D/(R*Cap)];
B = [D/L; 0];
C = [0 1];%绘制电容电压（即输出电压）曲线
C1 = [1 0];%绘制电感电流曲线

rank(ctrb(A, B))
rank(obsv(A, C))
rank(obsv(A, C1))

[num,den] = ss2tf(A, B*V, C, 0);%阶跃信号的幅值为V
g = tf(num,den)
step(g, 'b')

title('D=0.5');hold on;grid

[num1,den1] = ss2tf(A, B*V, C1, 0);
g1 = tf(num1,den1)
step(g1, 'g')
legend('电容电压（输出电压）', '电感电流')
\end{lstlisting}

控制台输出：
\begin{figure}[H]
	\centering
	\includegraphics[width=2.5in]{fig/console.eps}
\end{figure}
可以看出系统既能观又能控。
响应曲线：
\begin{figure}[H]
	\centering
	\includegraphics[width=3.8in]{fig/d0.5resp.eps}
\end{figure}
由图可知，电感电流连续；输出电压趋于稳定，稳态输出$u_{o}=\cfrac{0.5*50}{0.5-1}=-50\mathrm{V}$。
\subsection{理论公式验证}
通过控制变量，验证控制器输出值是否满足理论公式$U_{o}=\cfrac{DU_{i}}{D-1}$。
\paragraph{改变输入电压}
仅改变输入电压$V$,分别绘制$V=10\mathrm{V}$, $20\mathrm{V}$, $30\mathrm{V}$, $40\mathrm{V}$, $50\mathrm{V}$时输出电压的曲线。

MATLAB程序：
\begin{lstlisting}
D = [.3 .4 .5 .6 .7];%D为占空比
V = [10 20 30 40 50];%V为输入电压
style = ['b','y','--',':','black'];
L = 200*10^(-6);
R = 20;
Cap = 470*10^(-6);%电容值
C=[0 1];%C阵

for i=1:5
A = [0 (1-D(3))/L; -(1-D(3))/Cap -D(3)/(R*Cap)];
B = [D(3)/L; 0];
[num,den] = ss2tf(A, B*V(i), C, 0);%阶跃信号的幅值为V
step(tf(num,den), style(i))
title('输入电压变化对输出电压的影响');hold on;grid
end
legend('V=10', 'V=20','V=30', 'V=40', 'V=50')
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5in]{fig/Ui2Uo.eps}
\end{figure}

由图可知，输出电压与输入电压成线性关系。

\paragraph{改变占空比}
仅改变占空比$D$,分别绘制$D=0.3$, $0.4$, $0.5$, $0.6$, $0.7$时，输出电压的曲线。

MATLAB程序:
\begin{lstlisting}
D = [.3 .4 .5 .6 .7];%D为占空比
V = [10 20 30 40 50];%V为输入电压
style = ['b','y','--',':','black'];
L = 200*10^(-6);
R = 20;
Cap = 470*10^(-6);%电容值
C=[0 1];%C阵

for j=1:5
A = [0 (1-D(j))/L; -(1-D(j))/Cap -D(j)/(R*Cap)];
B = [D(j)/L; 0];
[num,den] = ss2tf(A, B*V(5), C, 0);%阶跃信号的幅值为V
step(tf(num,den), style(j))
title('占空比变化对输出电压的影响');hold on;grid
end
legend('D=0.3', 'D=0.4','D=0.5', 'D=0.6', 'D=0.7')
\end{lstlisting}
\begin{figure}[H]
	\centering
	\includegraphics[width=5in]{fig/D2Uo.eps}
\end{figure}

经验算，输出电压满足理论公式。因此改变占空比即可控制输出电压的稳态值，达到控制效果。从图中还可以观察到，占空比为0.5时，输出电压幅值正好与输入电压幅值相等；占空比大于0.5时，前者大于后者；占空比小于0.5时，前者小于后者。因而Buck-Boost电路可以通过调节占空比，任意控制输出电压。由此证明了状态平均法建模的正确性。

\section{研究体会}
这次大作业给我一个将所学理论付于实践的机会。起初在选模型的时候我是茫然的，后来在一节电力电子技术课上老师讲了Buck-Boost变换器的结构，我就对这种看似简单但是高效的电路产生了兴趣，随即就开始查找资料，一个模型的轮廓就逐渐清晰起来。虽然工程应用上的电路很复杂（还需考虑纹波电压、晶体管损耗等非线性的干扰），但是我熟悉了建模的基本步骤，顺便锻炼了使用$\mathrm{L^{A}T_{E}X}$语法写作的技能，收获良多。
\end{document}

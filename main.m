D = [.3 .4 .5 .6 .7];%D为占空比
V = [10 20 30 40 50];%V为输入电压
style = ['b','y','--',':','black'];
L = 200*10^(-6);
R = 20;
Cap = 470*10^(-6);%电容值
C=[0 1];%C阵

for i=1:5
A = [0 (1-D(3))/L; -(1-D(3))/Cap -D(3)/(R*Cap)];
B = [D(3)/L; 0];
[num,den] = ss2tf(A, B*V(i), C, 0);%阶跃信号的幅值为V
step(tf(num,den), style(i))
title('输入电压变化对输出电压的影响');hold on;grid
end
legend('V=10', 'V=20','V=30', 'V=40', 'V=50')

figure

for j=1:5
A = [0 (1-D(j))/L; -(1-D(j))/Cap -D(j)/(R*Cap)];
B = [D(j)/L; 0];
[num,den] = ss2tf(A, B*V(5), C, 0);%阶跃信号的幅值为V
step(tf(num,den), style(j))
title('占空比变化对输出电压的影响');hold on;grid
end
legend('D=0.3', 'D=0.4','D=0.5', 'D=0.6', 'D=0.7')